package tw.bookshop;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookShopController
{

    @GetMapping("/application")
    public Book applicationName(){
        Book book = new Book();
        book.setName("Book Shop");
        return book;
    }
}
